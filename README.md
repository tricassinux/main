# Tricassinux Dépôt général pour l'administration et les idées

Dépôt pour l'organisation générale de Tricassinux ou pour tester des idées.

Les membres et amis de Tricassinux sont invités à déposer des bouts de code, idées, propositions ici.

Une idée, un projet mourra migrer sur son propre espace si celui-ci prend une dimension qui le nécessiterait.


# Sommaire

1. Serveur-TCX : Proposition d'organisation du serveur de TCX pour usage interne et pour favoriser l'adoption du logiciel libre et open source pour toutes et tous.


# Pour adhérer, soutenir ou contacter Tricassinux

* Adhésions, dons, achats d'objet publicitaires : https://www.helloasso.com/associations/tricassinux/

* Notre forum : https://tricassinux.org/forum

* Par courriel : contact@tricassinux.org.

* Notre site : https://tricassinux.org

